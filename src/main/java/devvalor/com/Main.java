package devvalor.com;

import devvalor.com.javasolutions.AddTwoNumbers1228;
import devvalor.com.javasolutions.ListNode;

public class Main {
    public static void main(String[] args) {

        System.out.println("Hello world!");

        AddTwoNumbers1228 solution = new AddTwoNumbers1228();
        ListNode list1 = new ListNode(2);
        list1.next = new ListNode(4);
        list1.next.next = new ListNode(3);

        ListNode list2 = new ListNode(5);
        list2.next = new ListNode(6);
        list2.next.next = new ListNode(4);

        ListNode result = solution.addTwoNumbers(list1, list2);
        while(result != null){
            System.out.println(result.val);
            result = result.next;
        }
    }
}