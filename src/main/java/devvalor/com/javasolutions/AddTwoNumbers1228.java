package devvalor.com.javasolutions;

public class AddTwoNumbers1228 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode resultPreHead = new ListNode(-1);
        ListNode currentResult = resultPreHead;

        Boolean shouldAddOne = false;

        while (l1 != null || l2 != null) {
            Integer currValue = 0;

            if (shouldAddOne) {
                currValue++;
            }

            if (l1 != null && l2 != null) {
                currValue += l1.val + l2.val;
            } else if (l1 != null) {
                currValue += l1.val;
            } else {
                currValue += l2.val;
            }


            if (currValue < 10) {
                currentResult.next = new ListNode(currValue);
                shouldAddOne = false;
            } else {
                currentResult.next = new ListNode(currValue % 10);
                shouldAddOne = true;
            }

            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;
            currentResult = currentResult.next;
        }

        if(shouldAddOne){
            currentResult.next = new ListNode(1);
        }

        return resultPreHead.next;
    }

}
