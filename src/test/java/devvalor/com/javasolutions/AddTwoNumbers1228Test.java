package devvalor.com.javasolutions;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static devvalor.com.javasolutions.TestUtils.convertToArray;
import static devvalor.com.javasolutions.TestUtils.listToLinkedList;
import static org.assertj.core.api.Assertions.assertThat;

class AddTwoNumbers1228Test {

    @Test
    public void shouldAddTwoNumbers(){
        AddTwoNumbers1228 solution = new AddTwoNumbers1228();
        ListNode list1 = listToLinkedList(List.of(2, 4, 3));
        ListNode list2 = listToLinkedList(List.of(5, 6, 4));

        ListNode listA = listToLinkedList(List.of(9,9,9,9,9,9,9));
        ListNode listB = listToLinkedList(List.of(9,9,9,9));

        ListNode result = solution.addTwoNumbers(list1, list2);
        ListNode resulAB = solution.addTwoNumbers(listA, listB);

        assertThat(convertToArray(result)).isEqualTo(List.of(7,0,8));
        assertThat(convertToArray(resulAB)).isEqualTo(List.of(8,9,9,9,0,0,0,1));
    }
}