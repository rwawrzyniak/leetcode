package devvalor.com.javasolutions;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {
    public static ArrayList<Integer> convertToArray(ListNode listNode){
        ArrayList<Integer> list = new ArrayList<>();
        while(listNode != null){
            list.add(listNode.val);
            listNode = listNode.next;
       }
        return list;
    }

    public static ListNode listToLinkedList(List<Integer> list){
         ListNode preHead = new ListNode(-1);
         ListNode current = preHead;
         for(int value: list){
             current.next = new ListNode(value);
             current = current.next;
         }

        return preHead.next;
    }
}
